var newForm = {
    tbody: null,
    template: null,
    counter: 0,
    addRow: function () {
        var tr = document.createElement("tr");
        tr.innerHTML = this.template.replace(/\*index\*/g, this.counter);
        tr.id = "template_" + this.counter;
        this.tbody.appendChild(tr);
        this.counter++;
    },
    deleteRow: function (id) {
       this.tbody.querySelector('#template_' + id).remove();
    }
};

document.addEventListener("DOMContentLoaded", function () {
    newForm.tbody = document.querySelector('#templateTbody');
    newForm.counter = newForm.tbody.querySelectorAll('tr').length;
    newForm.template = document.querySelector('#row_template').innerHTML;
}, false);