<?php

require_once '../db/TemplateTable.php';
require_once '../db/TemplateFieldTable.php';

class Model
{
    private static $instance;

    private $connection;

    private function __construct()
    {
        $this->connection = new mysqli('localhost', 'root', null, 'form_builder');
    }

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getTable($name)
    {
        return new $name($this->connection);
    }
}
