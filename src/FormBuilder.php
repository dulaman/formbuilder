<?php

require_once 'Form.php';

class FormBulider implements Countable, ArrayAccess, Iterator
{
    private $position = 0, $_formFields = [], $url, $method;

    public function count()
    {
        return count($this->_formFields);
    }

    public function __construct($url, $method)
    {
        $this->url = $url;
        $this->method = $method;
        $this->position = 0;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function offsetExists($offset)
    {
        return isset($this->_formFields[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_formFields[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (!($value instanceof Form)) {
            throw new Exception('Not form instance');
        }
        if (is_null($offset)) {
            $this->_formFields[] = $value;
        } else {
            $this->_formFields[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->_formFields[$offset]);
    }

    public function toArray()
    {
        return $this->_formFields;
    }

    public function current()
    {
        return $this->_formFields[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return isset($this->_formFields[$this->position]);
    }

    public static function serialize($array, $entityClassName)
    {
    }

    public function add($name, $label, $description, $default, $attrs, $type)
    {
        $this[] = new Form($name, $label, $description, $default, $attrs, $type);
    }

    public function buildFromCollection($collection)
    {
        foreach ($collection as $entity) {
            $this->add($entity->name, $entity->name, $entity->description, $entity->default, [], $entity->type);
        }

        return $this;
    }
}
