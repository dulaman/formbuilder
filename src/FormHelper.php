<?php

require_once 'FormBuilder.php';

class FormHelper
{
    public static function baseFormDate($name)
    {
        return "<input id='".$name."' type='text' name='base[".$name."]'></input>";
    }

    public static function baseOptions()
    {
        return "<option value='text'>Krótkie pole tekstowe</option>"
            ."<option value='textarea'>Pole tekstowe</option>"
            ."<option value='checkbox'>Pole wyboru</option>"
        ;
    }

    public static function baseNameForm($id)
    {
        return "<input id='".$id."_name' type='text' name='".$id."[name]'></input>";
    }

    public static function baseSelectForm($id)
    {
        return "<select  id='".$id."_type' name='".$id."[type]'>".
                self::baseOptions().
                '</select>';
    }

    public static function baseDescriptionForm($id)
    {
        return "<input id='".$id."_description' type='text' name='".$id."[description]'></input>";
    }

    public static function baseDefaultValueForm($id)
    {
        return "<input id='".$id."_default' type='text' name='".$id."[default]'></input>";
    }
}
