<?php

require_once 'FieldTypeEnum.php';

class Form
{
    private $name, $label, $description, $default, $attrs, $type;

    public function __construct($name, $label, $description, $default, $attrs, $type)
    {
        $this->name = $name;
        $this->label = $label;
        $this->description = $description;
        $this->default = $default;
        $this->attrs = $attrs;
        $this->type = $type;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAttrs()
    {
        return $this->attrs;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setAttrs($attrs)
    {
        $this->attrs = $attrs;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setDefault($default)
    {
        $this->default = $default;
    }

    public function getHTML()
    {
        $htmlArray = FiledTypeEnum::getByKey($this->type);
        $html = $htmlArray['begin'].' ';
        foreach ($this->attrs as $key => $value) {
            $html .= $key."='".$value."' ";
        }
        $html .= "placeholder='".$this->default."' id='".$this->name."' name='".$this->name."'>".($this->type === 'textarea' ? $this->default : '').$htmlArray['end'];

        return $html;
    }

    public function getLabelAsHTML()
    {
        return "<label for='".$this->name."'>".$this->label.'</label>';
    }
}
