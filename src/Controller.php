<?php

require_once 'Model.php';
require_once '../db/Template.php';
require_once '../db/TemplateField.php';
require_once 'FormBuilder.php';

class Controller
{
    public static function redirect($routeName)
    {
        header('Location: '.$routeName.'.php');
        exit();
    }

    public static function generateAnchor($route, $content, $params = [], $newTab = false)
    {
        echo "<a href='".$route.'.php'.(empty($params) ? '' : '?'.http_build_query($params))."'".($newTab ? "traget='_blank'" : '').'>'.$content.'</a>';
    }

    public static function renderView($viewName, $params = [])
    {
        $params = array_merge($params, ['generateAnchor' => ['Controller', 'generateAnchor']]);
        include_once '../views/'.$viewName.'.php';
    }

    public static function getModel()
    {
        return Model::getInstance();
    }

    public static function mainAction()
    {
        $model = self::getModel();
        $template = $model->getTable('TemplateTable');
        self::renderView('main', [
            'entities' => $template->fetchAll(),
        ]);
    }

    public static function newFormAction()
    {
        $model = self::getModel();
        self::renderView('newForm', ['url' => 'createForm.php']);
    }

    public static function createFormAction($post)
    {
        $model = self::getModel();
        $entity = Template::fromArray($post['base']);

        $id = $model->getTable('TemplateTable')->save($entity);

        unset($post['base']);
        foreach ($post as $value) {
            $temp = TemplateField::fromArray($value);
            $temp->id_template = $id;
            $model->getTable('TemplateFieldTable')->save($temp);
        }
        self::redirect('main');
    }

    public static function newFormDBAction($id)
    {
        if ($id === null) {
            self::redirect('main');
        }
        $model = self::getModel();
        $template = $model->getTable('TemplateTable');
        $entity = $template->find((int) $id);
        $templateFields = $model->getTable('TemplateFieldTable');
        $form = (new FormBulider('createFormDB.php', 'POST'))->buildFromCollection($templateFields->findAllByKey('id_template', (int) $id));
        self::renderView('newFormDB', [
            'url' => 'createFormDB.php',
            'title' => $entity[0]->name,
            'form' => $form,
        ]);
    }

    public static function createFormDBAction($post)
    {
        self::redirect('main');
    }
}
