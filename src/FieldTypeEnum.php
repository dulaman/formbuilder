<?php

class FiledTypeEnum
{
    const types = [
        'text' => [
            'begin' => "<input type='text'",
            'end' => '</input>',
        ],
        'checkbox' => [
            'begin' => "<input type='checkbox'",
            'end' => '</input>',
        ],
        'textarea' => [
            'begin' => '<textarea',
            'end' => '</textarea>',
        ],
    ];

    public static function getByKey($key)
    {
        $types = self::types;

        return $types[$key];
    }
}
