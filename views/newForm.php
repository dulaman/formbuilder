<?php require_once '../src/FormHelper.php'; ?>
<!DOCTYPE html>
<?php $title = "SimpleFormBuilder - Nowy"; ?>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <script type="text/html" id="row_template">
            <td>
                <?php echo FormHelper::baseNameForm("*index*"); ?>
            </td>
            <td>
                <?php echo FormHelper::baseSelectForm("*index*"); ?>
            </td>
            <td>
                <?php echo FormHelper::baseDescriptionForm("*index*"); ?>
            </td>
            <td>
                <?php echo FormHelper::baseDefaultValueForm("*index*"); ?>
            </td>
            <td>
                <button type="button" onclick="newForm.deleteRow(*index*)">Usuń pole</button>
            </td>
        </script>
        <script type="text/javascript" src="../js/newForm.js"></script>
</head>
<body>
    <h1><?php echo $title; ?></h1>
    <form action="<?php echo $params['url']; ?>" method="POST">
        <table>
            <tbody>
                <?php foreach(["Nazwa" => "name", "Opis" => "description"] as $key => $value) { ?>
                    <tr>
                        <th>
                            <label for="<?php echo $value; ?>"><?php echo $key; ?></label>
                        </th>
                        <td>
                            <?php echo FormHelper::baseFormDate($value); ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <th>
                        Nazwa
                    </th>
                    <th>
                        Typ
                    </th>
                    <th>
                        Opis
                    </th>
                    <th>
                        Wartość domyślna
                    </th>
                </tr>
            </thead>
            <tbody id="templateTbody">
                <tr id="template_0">
                    <td>
                        <?php echo FormHelper::baseNameForm(0); ?>
                    </td>
                    <td>
                        <?php echo FormHelper::baseSelectForm(0); ?>
                    </td>
                    <td>
                        <?php echo FormHelper::baseDescriptionForm(0); ?>
                    </td>
                    <td>
                        <?php echo FormHelper::baseDefaultValueForm(0); ?>
                    </td>
                    <td>
                        <button type="button" onclick="newForm.deleteRow(0)">Usuń pole</button>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4">
                        <button type="button" onclick="newForm.addRow()">Dodaj pole</button>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div>
            <button type="submit">Zapisz</button>
        </div>
    </form>
</body>
</html>

