<!DOCTYPE html>
<?php $title = $params['title']; ?>
<?php $form = $params['form']; ?>
<html>
    <head>
        <title><?php echo $title; ?></title>
</head>
<body>
    <h1><?php echo $title; ?></h1>
    <form action="<?php echo $form->getUrl(); ?>" method="<?php echo $form->getMethod(); ?>">
        <table>
            <thead>
                <tr>
                    <th>
                        Nazwa pola
                    </th>
                    <th>
                        Pole
                    </th>
                    <th>
                        Opis
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($form as $singleForm) { ?>
                    <tr>
                        <td>
                            <?php echo $singleForm->getLabelAsHTML(); ?>
                        </td>
                        <td>
                            <?php echo $singleForm->getHTML(); ?>
                        </td>
                        <td>
                            <?php echo $singleForm->getDescription(); ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <div>
            <button type="submit">Zapisz</button>
        </div>
    </form>
</body>
</html>

