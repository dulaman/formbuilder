<!DOCTYPE html>
<?php $title = "SimpleFormBuilder - Nowy"; ?>
<?php $headers = ["Nazwa" => "name", "Opis" => "description"]; ?>
<html>
    <head>
        <title><?php $title ?></title>
    </head>
    <body>
        <h1><?php $title; ?></h1>
        <table>
            <thead>
                <tr>
                <?php foreach($headers as $label => $value) { ?>
                     <th><?php echo $label; ?></th>
                <?php } ?>
                     <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($params['entities']->toArray() as $value) {?>
                    <tr>
                    <?php foreach($headers as $lable => $name) { ?>
                        <td>
                    <?php echo $value->{$name}; ?>
                        </td>
                    <?php } ?>
                        <td>
                            <?php forward_static_call_array($params['generateAnchor'], ["newFormDB", "<button type='button'>Nowy dokument</button>", ["id" => $value->id]]); ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="<?php echo count($headers)+1; ?>">
                        <?php forward_static_call_array($params['generateAnchor'], ["newForm", "<button type='button'>+ Dodaj formularz</button>"]); ?>
                    </td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>
