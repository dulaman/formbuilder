<?php

require_once 'Entity.php';
require_once 'Collection.php';

class SimpleTable
{
    protected $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function fetchAll()
    {
        $result = $this->connection->query('SELECT * FROM `'.$this->_tableName.'`');

        return Collection::serialize($result->fetch_all(MYSQLI_ASSOC), preg_replace('/Table/', '', get_class($this)));
    }

    public function fetchFields()
    {
        $result = $this->connection->query('SELECT * FROM `'.$this->_tableName.'` LIMIT 1');

        return $result->fetch_fields();
    }

    public function find($id)
    {
        $result = $this->connection->query('SELECT * FROM `'.$this->_tableName."` WHERE id = '".$this->connection->escape_string($id)."' LIMIT 1");

        return Collection::serialize($result->fetch_all(MYSQLI_ASSOC), preg_replace('/Table/', '', get_class($this)));
    }

    public function findAllByKey($key, $value)
    {
        $result = $this->connection->query('SELECT * FROM `'.$this->_tableName.'` WHERE `'.$key."` = '".$this->connection->escape_string($value)."'");

        return Collection::serialize($result->fetch_all(MYSQLI_ASSOC), preg_replace('/Table/', '', get_class($this)));
    }

    public function save(Entity $entity)
    {
        return $entity->insert($this->connection, $this->fetchFields());
    }
}
