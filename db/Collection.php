<?php

require_once 'Entity.php';
require_once 'Template.php';
require_once 'TemplateField.php';

class Collection implements Countable, ArrayAccess, Iterator
{
    private $position = 0;
    private $_entities = [];

    public function count()
    {
        return count($this->_entities);
    }

    public function __construct()
    {
        $this->position = 0;
    }

    public function offsetExists($offset)
    {
        return isset($this->_entities[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_entities[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (!($value instanceof Entity)) {
            throw new Exception('Not entity instance');
        }
        if (is_null($offset)) {
            $this->_entities[] = $value;
        } else {
            $this->_entities[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->_entities[$offset]);
    }

    public function toArray()
    {
        return $this->_entities;
    }

    public function current()
    {
        return $this->_entities[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return isset($this->_entities[$this->position]);
    }

    public static function serialize($array, $entityClassName)
    {
        $instance = new static();
        if ($array !== null) {
            foreach ($array as $value) {
                $instance->offsetSet(null, forward_static_call([$entityClassName, 'fromArray'], $value));
            }
        }

        return $instance;
    }
}
