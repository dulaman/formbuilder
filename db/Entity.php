<?php

class Entity
{
    public function __get($name)
    {
        return $this->{$name};
    }

    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public static function fromArray($array)
    {
        $instance = new static();
        foreach ($array as $key => $value) {
            $instance->{$key} = $value;
        }

        return $instance;
    }

    public function insert(mysqli $connection, $fields)
    {
        $sql = 'INSERT INTO `'.$this->_tableName.'` SET ';

        foreach ($fields as $value) {
            if ($value->name === 'id') {
                continue;
            }
            $sql .= '`'.$value->name."` = '".
                    $connection->escape_string($this->{$value->name})."'";
            if (end($fields) !== ($value)) {
                $sql .= ', ';
            }
        }
        if ($connection->query($sql) === false) {
            throw new Exception($connection->error);
        }

        return $connection->insert_id;
    }
}
