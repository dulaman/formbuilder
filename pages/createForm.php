<?php

require_once '../src/Controller.php';

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    throw new Exception('HTTP Method Not Allowed!');
}

Controller::createFormAction($_POST);
